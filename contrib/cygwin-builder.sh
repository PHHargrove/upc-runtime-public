#!/bin/bash
#
# This script builds the Cywin 1.7.x package for BUPC.
# The only argument is the version number (in form X.Y.Z).
#
VERSION=$1
if test -z "$VERSION"; then
  echo Missing version argument
  exit 1
fi
ORIGDIR=`pwd -P`
if test ${VERSION} = nightly; then
  NAME=berkeley_upc_runtime
else
  NAME=berkeley_upc-${VERSION}
fi
set -x
set -e
#
TOPDIR=`mktemp -d ${TMPDIR:-/tmp}/${NAME}-XXXXXX`
cd ${TOPDIR}
# Fetch tarball
if curl -h >/dev/null 2>&1; then
  fetch_cmd='curl -LsS'
else
  fetch_cmd='wget -nv -O -'
fi
eval ${fetch_cmd} https://upc-bugs.lbl.gov/nightly/upc/${NAME}.tar.gz | tar xz
#
# Build the source release candidate
# (with multiconf, to get both debug and ndebug)
mkdir build
cd build
${TOPDIR}/${NAME}/configure \
	--enable-pthreads --enable-smp --enable-udp \
	--disable-mpi --disable-mpi-compat --prefix=/usr/local/upc \
	gasnet_cv_datacache_line_size=128 CC=gcc-4 CXX=g++-4
#
#  Edit {dbg,opt}/upcc.conf and remove full paths from any binaries
#  in $SYSTEMROOT
#  Example: "/cygdrive/c/Windows/system32/nslookup" should become just "nslookup"
SYSROOT=`cygpath "$SYSTEMROOT"`
perl -pi -e "s,$SYSROOT.*/([^/]+),\1,;" -- `find . -name '*.conf'`
#
# Run a recursive grep to look for any remaining instances of $HOME or $SYSTEMROOT
# We expect to see {src,build}_dir variables that reference
# the build and source dirs
found=0
grep -rl --exclude=config.log -e "$HOME" -e "$SYSROOT" . && found=1
if test $found -eq 1; then
  set +x
  echo "Resolve the '$HOME' or '$SYSROOT' occurances in the files above"
  exit 1
fi
#
# Build it
#
make all
UPCC_NORC=1 make check
#
# Install and copy in examples, using $TOPDIR as a DESTDIR
#
make install DESTDIR=${TOPDIR}
cd ${TOPDIR}/usr/local/upc/
cp -R ${TOPDIR}/${NAME}/upc-examples .
cd upc-examples
find . -name harness.conf\* | xargs rm -f
find . -name CVS | xargs rm -Rf
chmod -R 755 .
#
# Create post-install script
#
cd ${TOPDIR}
mkdir -p etc/postinstall
cat > etc/postinstall/berkeley_upc.sh <<-'__EOF'
	#!/bin/sh
	
	cat >>/etc/profile <<_EOF
	if test -f /etc/motd ; then
	  /usr/bin/cat /etc/motd
	fi
	if test -f /etc/upccrc -a ! -f ~/.upccrc ; then
	  /usr/bin/cp /etc/upccrc ~/.upccrc
	  /usr/bin/chmod u+rw ~/.upccrc
	  if test -f /usr/local/upc/upc-examples/hello.upc ; then
	    /usr/bin/cp /usr/local/upc/upc-examples/hello.upc ~
	  fi
	fi
	_EOF
	
	cat >>/etc/csh.login <<_EOF
	if ( -f /etc/motd ) then
	  /usr/bin/cat /etc/motd
	endif
	if ( -f /etc/upccrc && ! -f ~/.upccrc ) then
	  /usr/bin/cp /etc/upccrc ~/.upccrc
	  /usr/bin/chmod u+rw ~/.upccrc
	  if ( -f /usr/local/upc/upc-examples/hello.upc ) then
	    /usr/bin/cp /usr/local/upc/upc-examples/hello.upc ~
	  endif
	endif
	_EOF
	
	echo 'setenv PATH "/usr/local/upc/bin:$PATH"' >> /etc/csh.login
	echo 'PATH="/usr/local/upc/bin:$PATH"' >> /etc/profile
	
	echo 'setenv MANPATH "/usr/local/upc/man:$MANPATH"' >> /etc/csh.login
	echo 'MANPATH="/usr/local/upc/man:$MANPATH"' >> /etc/profile
	
	touch /etc/motd
	cat >>/etc/motd <<_EOF
	Welcome to Cygwin!
	
	Berkeley UPC v@VERSION@ is installed in: 
	   /usr/local/upc/bin/upcc
	If you have an internet connection, you can try it right now, eg: 
	     upcc hello.upc
	     upcrun -np 4 hello
	
	Sample codes are available in: /usr/local/upc/upc-examples
	Documentation: 'man upcc', 'upcc --help' or https://upc.lbl.gov
	
	_EOF
	chmod a+r /etc/motd
	
	touch /etc/upccrc
	cat >>/etc/upccrc <<_EOF
	######################################################################
	#  Config settings for the Berkeley UPC compiler
	######################################################################
	
	# online UPC translators
	#translator = http://upc-translator.lbl.gov/upcc-@VERSION@.cgi 
	#translator = http://upc-translator.lbl.gov/upcc-stable.cgi 
	#translator = http://upc-translator.lbl.gov/upcc-yesterday.cgi 
	#translator = http://upc-translator.lbl.gov/upcc-nightly.cgi 
	
	# default upcc options to use
	default_options = -smart-output -nolink-cache -network smp -pthreads -shared-heap=64MB -nosize-warn
	
	_EOF
	chmod a+r /etc/upccrc
	
__EOF
perl -pi -e "s/\@VERSION\@/${VERSION}/;" -- etc/postinstall/berkeley_upc.sh
chmod 550 etc/postinstall/berkeley_upc.sh
#
# build the tarball
#
tar cf ${NAME}-1.tar etc/postinstall/berkeley_upc.sh usr/local/upc
bzip2 -v ${NAME}-1.tar
set +x
FILE=${NAME}-1.tar.bz2
SIZE=`du -b $FILE | cut -f1`
MD5=`md5sum $FILE | cut -d' ' -f1`
mv ${FILE} ${ORIGDIR}/
cat <<-_EOF
	========vv======== cut here ========vv========
	@ berkeley_upc
	sdesc: "Berkeley UPC compiler"
	ldesc: "Berkeley UPC compiler: https://upc.lbl.gov"
	category: Devel Base 0_BerkeleyUPC
	requires: cygwin gcc gcc-core gcc-g++ gawk make binutils ash gzip grep tar perl bind
	version: ${VERSION}-1
	install: release/berkeley_upc/${FILE} ${SIZE} ${MD5}
	========^^======== cut here ========^^========
_EOF
echo "Cygwin package ${FILE} is ready.  The setup.ini fragment appears above."
#
exit 0

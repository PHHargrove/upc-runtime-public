#!/bin/sh

# versioning information

packagename="berkeley_upc"
packageversion="2.18.0"

#expect_darwin_version="7"  # 10.3 (Panther)
#expect_cc_version="gcc version 3.3"   # Xcode 1.x
#expect_hardware_version="Power Macintosh"

#expect_darwin_version="8"  # 10.4 (Tiger)
#expect_cc_version="gcc version 4.0.0" # Xcode 2.0-2.1
#expect_cc_version="gcc version 4.0.1" # Xcode 2.2-2.5
#expect_hardware_version="Power Macintosh"
#expect_hardware_version="i386"

#expect_darwin_version="9"  # 10.5 (Leopard)
#expect_cc_version="gcc version 4.0.1" # Xcode 3.0
#expect_hardware_version="Power Macintosh"
#expect_hardware_version="i386"

#expect_darwin_version="10" # 10.6 (Snow Leopard)
#expect_cc_version="gcc version 4.2.1" # Xcode 3.2
#expect_hardware_version="Power Macintosh"
#expect_hardware_version="i386"

#expect_darwin_version="11" # 10.7 (Lion)
#expect_cc_version="gcc version 4.2.1" # Xcode 4.1+
#expect_hardware_version="x86_64"

#expect_darwin_version="12" # 10.8 (Mountain Lion)
#expect_cc_version="clang version 4.0" # Xcode 4.1+
#expect_hardware_version="x86_64"

# parse arguments
for arg ; do
  case "$arg" in
      -t) 
	  DOIT="echo Would run:"
          echo RUNNING IN TEST-ONLY MODE
	  ;;
      LOCALROOT=*) 
          eval $arg
	  echo "Using LOCALROOT: $LOCALROOT"
          ;;
      *)  echo "Unrecognized argument: $arg"
          echo "Usage: $0 [options]"
	  echo "Supported options:"
          echo "  -t  "
	  echo "      run in test-only mode - make no actual changes"
          echo "  LOCALROOT=/prefix/to/use  "
	  echo "      perform non-privileged install in provided prefix"
          exit 1
	  ;;
  esac
done

# get rooted
if test -z "$IN_BUPC_INSTALLER" ; then
  clear
  IN_BUPC_INSTALLER=1 ; export IN_BUPC_INSTALLER
  echo ""
  echo " ************** Berkeley UPC installer **************"
  echo ""
  if test "`whoami`" != "root" -a "$LOCALROOT" = "" -a "$DOIT" = ""; then
    echo "Please enter your password to proceed:"
    /usr/bin/sudo "$0" "$@"
    result=$?
  else
    "$0" "$@"
    result=$?
  fi
  if test "$result" = "0" ; then
    echo "INSTALLER OPERATION SUCCESSFUL"
    exit 0
  else
    echo "INSTALLER OPERATION FAILED"
    exit 1
  fi
fi

mydir=`/usr/bin/dirname "$0"`
mydir=`cd "$mydir" && /bin/pwd`
umask 022

# hardware version check
hardware_version=`/usr/bin/uname -m 2>&1`
echo "Architecture: $hardware_version"
# we allow ppc installer on i386 hardware (works via rosetta), but not vice-versa
if test "$hardware_version" = "Power Macintosh" -a "$expect_hardware_version" = "i386" ; then
  echo " "
  echo "ERROR: Unexpected architecture - this installer expects architecture $expect_hardware_version"
  echo "Please download the correct binary installer for your OS version from https://upc.lbl.gov"
  echo " "
  exit 1
fi

# kernel version check
darwin_version=`/usr/bin/uname -r 2>&1`
echo "Darwin Kernel version: $darwin_version"
if test -z "`echo $darwin_version | grep ^$expect_darwin_version`" ; then
  echo " "
  echo "ERROR: Unexpected kernel version - this installer expects darwin version $expect_darwin_version"
  echo "Please download the correct binary installer for your OS version from https://upc.lbl.gov"
  echo " "
  exit 1
fi

# cc version check
cc_version=`/usr/bin/cc -v 2>&1 | grep version`
echo "/usr/bin/cc is: $cc_version"
if test -z "`echo \"$cc_version\" | grep \"$expect_cc_version\"`" ; then
  echo " "
  echo "ERROR: Unexpected compiler version"
  echo "This installer expects compiler '$expect_cc_version'"
  echo "Please download the correct binary installer for your Xcode version"
  echo "from https://upc.lbl.gov or you may consider building from sources."
  echo " "
  exit 1
fi

# uninstall to ensure we start with a clean slate
uninstaller="$mydir/uninstall.command"
result=1
if test -x "$uninstaller" ; then
  echo "Running uninstaller..."
  $uninstaller "$@"
  result=$?
else
  echo "ERROR: failed to find uninstall script"
fi
if test "$result" != "0" ; then
  echo "ERROR: uninstall failed!"
  exit 1
fi

if test "$LOCALROOT" ; then
  installroot="$LOCALROOT"
else
  installroot="/usr/local"
fi
mkdir -p "$installroot" || exit 1
installroot=`cd "$installroot" && /bin/pwd`
installdir_generic="$installroot/$packagename"
installdir="$installroot/$packagename-$packageversion"
globalbin="/usr/bin"
globalman="/usr/share/man"

upcr_tarball="$mydir/bupc_runtime-archive"
trans_tarball="$mydir/bupc_translator-archive"

$DOIT set -x -e

$DOIT mkdir -p "$installdir"

# Runtime install
if test -f "$upcr_tarball" ; then
  echo --- Installing Berkeley UPC Runtime ---
  if test "$DOIT" ; then
    $DOIT "cd $installdir && /usr/bin/gunzip -c $upcr_tarball | /usr/bin/tar xvpf - "
  else 
    ( cd "$installdir" && /usr/bin/gunzip -c "$upcr_tarball" | /usr/bin/tar xvpf - )
  fi
  $DOIT find "$installdir" \( -name '*.mak' -o -name '*.conf' \) \
     -exec echo Patching {}... \; \
     -exec /usr/bin/perl -pi -e 's@/tmp/###BININSTALL_PREFIX###@'"$installdir"'@g' {} \;
  have_runtime=1
fi

# Translator install
if test -f "$trans_tarball" ; then
  echo --- Installing Berkeley UPC Translator ---
  if test "$DOIT" ; then
    $DOIT "cd $installdir && /usr/bin/gunzip -c $trans_tarball | /usr/bin/tar xvpf - "
  else 
    ( cd "$installdir" && /usr/bin/gunzip -c "$trans_tarball" | /usr/bin/tar xvpf - )
  fi
  # point runtime to this translator
  for conffile in "$installdir/opt/etc/upcc.conf" "$installdir/dbg/etc/upcc.conf"; do
    $DOIT /usr/bin/perl -pi -e 's@^\s*translator\s*=.*$@translator = '"$installdir"'/trans/targ@;' "$conffile" || exit $?
  done
  have_trans=1
fi

if test "$have_trans" = "" -a "$have_runtime" = "" ; then
  echo "ERROR: failed to find install archives - install directory is corrupted or incomplete"
  exit 1
fi

if test "`whoami`" = "root" ; then
  $DOIT chown -R root "$installdir"
  $DOIT chgrp -R wheel "$installdir"
fi
$DOIT chmod -R a+rX,og-w "$installdir"
$DOIT ln -s "$installdir" "$installdir_generic"

# setup global links
if test -z "$LOCALROOT" ; then
  $DOIT ln -s "$installdir_generic/bin/upcc" "$globalbin/upcc"
  $DOIT ln -s "$installdir_generic/bin/upcrun" "$globalbin/upcrun"
  $DOIT ln -s "$installdir_generic/bin/upc_trace" "$globalbin/upc_trace"
  $DOIT ln -s "$installdir_generic/bin/upcdecl" "$globalbin/upcdecl"
  $DOIT ln -s "$installdir_generic/dbg/bin/gasnet_trace" "$globalbin/gasnet_trace"

  $DOIT ln -s "$installdir_generic/man/man1/upcc.1" "$globalman/man1/upcc.1"
  $DOIT ln -s "$installdir_generic/man/man1/upcrun.1" "$globalman/man1/upcrun.1"
  $DOIT ln -s "$installdir_generic/man/man1/upc_trace.1" "$globalman/man1/upc_trace.1"
  $DOIT ln -s "$installdir_generic/man/man1/upcdecl.1" "$globalman/man1/upcdecl.1"
fi

$DOIT set +x +e
  
echo "Install complete."
if test -z "$DOIT" ; then
  if test "$LOCALROOT" ; then
    EXEC_PREFIX="$installdir_generic/bin/"
  fi
  echo "---------------------------------------------------------------"
  echo " Berkeley UPC version $packageversion has been installed in:"
  echo "    $installdir "
  if test -z "$LOCALROOT" ; then
    echo " with convenience symlinks in $globalbin"
  fi
  echo ""
  if test "$have_trans"; then
    echo " You can try it now, eg:"
  else
    echo " If you have an internet connection, you can try it now, eg: "
  fi
  echo "    ${EXEC_PREFIX}upcc -pthreads $installdir_generic/upc-examples/hello.upc"
  echo "    ${EXEC_PREFIX}upcrun -np 4 ./a.out"
  echo ""
  echo " Sample codes are available in: $installdir/upc-examples"
  echo " Documentation:  'man upcc', 'upcc --help' or https://upc.lbl.gov"
  echo "---------------------------------------------------------------"
fi
exit 0

/* Parameter declared with UPC shared qualifier. */

#include <upc.h>

int decl_shared_parameter (shared int p)
{
  return p;
}

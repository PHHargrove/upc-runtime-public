/* Function definition has UPC shared qualified return type. */

#include <upc.h>

shared int return_shared_int (void)
{
  return 0;
}
